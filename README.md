# ks-client
## API

All the functions that accept a callback also return promises. So you can use the library in the usual callback style or promise style, whichever you prefer.

**Requires C gateway version >= 1.0.18 or JS gateway version >= 2.0.0.**

**Example**

```js
var KSClient = require("ks-client");
var constants = KSClient.constants;
var client = new KSClient;

var path = "/tmp/ks_gw_socket";

// this is equivalent to...
client.connect(path, function(err) {
  if (err) {
    // handle error
    return;
  }

  // handle success
});

// ...this
client.connect(path).then(function () {
  // handle success
}).catch(function (err) {
  // handle error
});
```

### new KSClient

Creates a new client object.

The client is an EventEmitter and it emits the following events.

#### Event: ipc-disconnected

Fired when the IPC gets disconnected. You will need to call `#connect` again after this.

#### Event: state

Fired when the state of the gateway changes.

Field | Type | Description
------|------|------------
state | int | Represents the state of the client. One of the **network state** constants from [constants.md](constants.md).
error | int | Possible error code. One of the **error** constants from [constants.md](constants.md). 

#### Event: notification

Fired when a notification is received.

Field | Type | Description
------|------|------------
address | string | Alphanumeric string.
payloadType | string | Type of payload. One of: "BINARY", "INT", "STRING", "PING", "PONG".
payload | int, Buffer or string | 32 bit integer when payloadType is "INT", Buffer when "BINARY", string otherwise.

#### Event: rid

Fired when your application receives its RID.

Field | Type | Description
------|------|------------
address | string | Alphanumeric string.
rid | string | The RID received.
secret | string | The secret of that RID.

#### Event: gateway-rid

Fired when the gateway receives its RIDs.

Field | Type | Description
------|------|------------
rid | string | The gateway RID.

### #connect(path, [callback])

Connect to a socket in the given address or path. For example, `/tmp/ks_gw_socket` or `localhost:12345`.

### #disconnect([callback])

Disconnects an established connection, if any.

### #register(address, version, customerId, channels, [callback])

Registers with the daemon. App will start receiving notifications and other events.

NOTE: If you need to update the channels, you have to first unregister nd then register again, with the different channels.

Argument | Type | Description
---------|------|------------
address | string | Alphanumeric string.
version | string | Alphanumeric string.
customerId | string | Your customer ID. Alphanumeric.
channels | Array | Array of strings describing the channels you wish to subscribe to.

### #unregister(address, version, customerId, [callback])

Unregisters from the daemon.

Argument | Type | Description
---------|------|------------
address | string | Alphanumeric string.
version | string | Alphanumeric string.
customerId | string | Your customer ID. Alphanumeric.

### #publish(payloadType, payload, [tag], [callback])

Publishes a message to the server.

Argument | Type | Description
---------|------|------------
payloadType | string | Type of payload. One of: "BINARY", "INT", "STRING", "PING", "PONG".
payload | int, Buffer or string | 32 bit integer when payloadType is "INT", Buffer when "BINARY", string otherwise.
tag | string | SRIKY WHAT WAS THIS AGAIN?

### #requestState([callback])

Request the state from the daemon. This will cause the `state` event to be emitted. The callback is run when the request has been written.

### #requestRid([callback])

Request your rid from the daemon. This will cause the `rid` event to be emitted. The callback is run when the request has been written.

### #requestAppId([callback])

Request the application ID from the daemon. Callback will be called with the application ID.

### #setNetworkAvailable(opts, [callback])

Set the availability of the network. `opts` is an object of the following format: 

```js
{
  state: constants.NETWORK_STATE_MOBILE_2G, // one of the network state constants
  mcc: "555", // optional, use with mobile networks
  mnc: "66", // optional, use with mobile networks
}
```

`mcc` and `mnc` are used for optimization of the connection. If they change, you should update the state.

### #setEngineEnabled(enabled, [callback])

Enable or disable the gateway.

Argument | Type | Description
---------|------|------------
enabled | boolean | 

### #setOrganizationSecret(secret, [callback])

Set the organization secret. This will only have an effect if it is called on an empty database before any applications have been registered.

Argument | Type | Description
---------|------|------------
secret | string | 

## Example use

A complete example that works similarly to the C SDK's `sample_publish` can be found in [src/lib/example.js](src/lib/example.js).
