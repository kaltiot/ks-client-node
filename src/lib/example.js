var KSClient = require("ks-client");
var constants = KSClient.constants;
var Promise = require("bluebird");

var ADDRESS = "js_example_address";
var VERSION = "js_example_version";
var CUSTOMER_ID = "js_example_customer_id";
var CHANNELS = ["js_example_1", "js_example_2"];

var client = new KSClient;
var isOnline;
var rid;
var notificationReceived;

client.on("state", function (data) {
  console.log("state changed");
  console.log("  state:", data.state);
  console.log("  error:", data.error);

  isOnline = data.state === constants.STATE_ONLINE;
});

client.on("rid", function (data) {
  console.log("rid received");
  console.log("  address:", data.address);
  console.log("  rid:", data.rid);

  rid = data.rid;
});

client.on("notification", function (notification) {
  console.log("notification received");
  console.log("  address:", notification.address);
  console.log("  payloadType:", notification.payloadType);
  console.log("  payload:", notification.payload);

  notificationReceived = true;
});

client.connect("/tmp/ks_gw_socket")
.then(function () {
  console.log("requesting state");
  return client.requestState();
})
.then(function () {
  console.log("setting network");
  return client.setNetworkAvailable({
    state: constants.NETWORK_STATE_MOBILE_2G,
    mcc: "555",
    mnc: "66"
  });
})
.then(function () {
  console.log("customer_id:", CUSTOMER_ID);
  return client.setEngineEnabled(true);
})
// .then(function () {
//   console.log("setting organization secret");
//   return client.setOrganizationSecret("something");
// })
.then(function () {
  console.log("registering");
  return client.register(ADDRESS, VERSION, CUSTOMER_ID, CHANNELS);
})
.tap(function () { console.log("waiting for online state"); })
.then(function waitForOnline() {
  return Promise.delay(100)
  .then(function () {
    if (isOnline)
      return;

    return waitForOnline();
  });
})
.then(function () {
  console.log("requesting rid");
  return client.requestRid();
})
.tap(function () { console.log("waiting for rid"); })
.then(function waitForRid() {
  return Promise.delay(100)
  .then(function () {
    if (rid)
      return;

    return waitForRid();
  });
})
.tap(function () { console.log("waiting for notification"); })
.then(function waitForNotification() {
  return Promise.delay(100)
  .then(function () {
    if (notificationReceived)
      return;

    return waitForNotification();
  });
})
.then(function () {
  console.log("unregistering");
  return client.unregister(ADDRESS, VERSION, CUSTOMER_ID);
})
.then(function () {
  console.log("disconnecting");
  return client.disconnect();
});
