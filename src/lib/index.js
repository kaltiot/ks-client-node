import Promise from "bluebird";
import { Socket } from "net";
import { EventEmitter } from "events";
import os from "os";
import _ from "lodash";

import * as utils from "ks-client/utils";
import ProtobufParserStream from "ks-client/ProtobufParserStream";

import * as protocol from "@kaltiot/ks-ipc/protocol";
import TLVStream from "@kaltiot/ks-ipc/TLVStream";
import IPCClient from "@kaltiot/ks-ipc/client";

import constants from "@kaltiot/ks-constants";
const PAYLOAD_TYPE_VALUES = _.values(protocol.IpcPayloadType);

class KSClient extends EventEmitter {
  constructor() {
    super();
    this._ipcClient = new IPCClient;
  }

  connect(address, callback) {
    return this.disconnect()
      .bind(this)
      .then(this._resetState)
      .then(_ => this._ipcClient.connect(address))
      .then(this._registerEvents)
      .asCallback(callback);
  }

  _resetState() {
    this._appIdRequest = null;
  }

  _registerEvents() {
    this._ipcClient.on("end", _ => {
      this.emit("ipc-disconnected");
    });

    this._ipcClient
      .pipe(new ProtobufParserStream(protocol))
      .on("data", message => {
        const handler = _.find(KSClient.handlers, { type: message.type });

        if (handler) {
          handler.fn.call(this, message);
        }
      });
  }

  disconnect(callback) {
    return this._ipcClient.disconnect().asCallback(callback);
  }

  register(address, version, customerId, channels, callback) {
    return Promise.try(() => {
      this._ensureIsString("address", address);
      this._ensureIsString("version", version);
      this._ensureIsString("customerId", customerId);

      channels.forEach(channel =>
        this._ensureIsString("channel", channel)
      );

      const buffer = protocol.encode({
        type: "REGISTRATION",
        registration: {
          type: "REGISTER",
          address: utils.stringToIPC(address),
          version: utils.stringToIPC(version),
          customerid: utils.stringToIPC(customerId),
          capability: channels //.map(utils.stringToIPC)
        }
      });

      return this._write(buffer);
    })
    .asCallback(callback);
  }

  _ensureIsString(field, value) {
    if (!_.isString(value)) {
      throw new Error(`${field} must be a string`);
    }
  }

  unregister(address, version, customerId, callback) {
    return Promise.try(() => {
      this._ensureIsString("address", address);
      this._ensureIsString("version", version);
      this._ensureIsString("customerId", customerId);

      const buffer = protocol.encode({
        type: "REGISTRATION",
        registration: {
          type: "UNREGISTER",
          address: utils.stringToIPC(address),
          version: utils.stringToIPC(version),
          customerid: utils.stringToIPC(customerId),
          capability: [],
        }
      });

      return this._write(buffer);
    })
    .asCallback(callback);
  }

  publish(payloadType, payload, tag, callback) {
    const args = arguments;

    return Promise.try(() => {
      if (args.length === 3 && typeof tag === "function") {
        callback = tag;
        tag = null;
      }

      payloadType = protocol.payloadTypeFromString(payloadType);
      payload = this._getPublishPayload(payloadType, payload);

      const buffer = protocol.encode({
        type: "PUBLISH",
        publish: {
          payload,
          type: payloadType,
          tag: utils.stringToIPC(tag),
        }
      });

      return this._write(buffer);
    })
    .asCallback(callback);
  }

  _getPublishPayload(payloadType, payload) {
    if (payloadType === protocol.IpcPayloadType.BINARY) {
      if (!_.isBuffer(payload)) {
        throw new Error("payload must be a Buffer for this payload type");
      }

      return payload;
    }

    else if (payloadType === protocol.IpcPayloadType.INT) {
      if (!_.isInteger(payload)
      ||  payload < -2147483648
      ||  payload > 2147483647) {
        throw new Error(
          "payload must be a 32 bit signed integer for this payload type"
        );
      }

      return utils.int32ToBufferLE(payload);
    }

    else if (payloadType === protocol.IpcPayloadType.STRING
         ||  payloadType === protocol.IpcPayloadType.PING
         ||  payloadType === protocol.IpcPayloadType.PONG) {
      if (!_.isString(payload)) {
        throw new Error("payload must be a string for this payload type");
      }

      return utils.stringToBuffer(payload);
    }
  }

  requestState(callback) {
    return Promise.try(() => {
      const buffer = protocol.encode({
        type: "STATE",
        state: {
          type: "REQUEST"
        }
      });

      return this._write(buffer);
    })
    .asCallback(callback);
  }

  requestRid(callback) {
    return Promise.try(() => {
      const buffer = protocol.encode({
        type: "RID",
        rid: {
          type: "REQUEST"
        }
      });

      return this._write(buffer);
    })
    .asCallback(callback);
  }

  requestAppId(callback) {
    return Promise.try(() => {
      if (!this._appIdRequest) {
        this._appIdRequest = this._createAppIdRequest(callback);
      }

      return this._appIdRequest.promise;
    })
    .asCallback(callback);
  }

  _createAppIdRequest(callback) {
    const buffer = protocol.encode({
      type: "APPLICATION_ID",
      application_id: {
        type: "REQUEST"
      }
    });

    const deferred = {};

    deferred.promise = new Promise((resolve, reject) => {
      deferred.resolve = resolve;
      deferred.reject = reject;
    });

    this._write(buffer)
      .catch(err => {
        deferred.reject(err);
        this._appIdRequest = null;
      });

    return deferred;
  }

  setNetworkAvailable(opts, callback) {
    return Promise.try(() => {
      opts = _.defaults(opts, {
        state: -1,
        mcc: undefined,
        mnc: undefined,
      });

      this._ensureIsUInt("opts.state", opts.state);
      this._ensureIsString("opts.mcc", opts.mcc);
      this._ensureIsString("opts.mnc", opts.mnc);

      const buffer = protocol.encode({
        type: "NETWORK",
        network: {
          type: opts.state,
          mcc: utils.stringToIPC(opts.mcc),
          mnc: utils.stringToIPC(opts.mnc),
        }
      });

      return this._write(buffer);
    })
    .asCallback(callback);
  }

  _ensureIsUInt(field, value) {
    if (!_.isInteger(value) || value < 0) {
      throw new Error(`${field} must be an unsigned integer`);
    }
  }

  setEngineEnabled(enabled, callback) {
    return Promise.try(() => {
      const buffer = protocol.encode({
        type: "ENGINE",
        engine: { enabled }
      });

      return this._write(buffer);
    })
    .asCallback(callback);
  }

  setOrganizationSecret(secret, callback) {
    return Promise.try(() => {
      this._ensureIsString("secret", secret);

      const buffer = protocol.encode({
        type: "SUB_ORGANISATION",
        sub_org: {
          secret: utils.stringToIPC(secret),
        }
      });

      return this._write(buffer);
    })
    .asCallback(callback);
  }

  _write(data) {
    return this._ipcClient.write(data);
  }
}

KSClient.handlers = [{
  type: protocol.IPCMessage.Type.STATE,
  fn: function (message) {
    const state = message.state.response.state;
    const error = message.state.response.error;

    this.emit("state", { state, error });
  },
}, {
  type: protocol.IPCMessage.Type.NOTIFICATION,
  fn: function (message) {
    const payloadType = message.notification.type;
    let payload = message.notification.payload.toBuffer();

    if (!_.includes(PAYLOAD_TYPE_VALUES, payloadType)) {
      console.error("invalid payload type", payloadType);
      return;
    }

    if (payloadType === protocol.IpcPayloadType.STRING
    ||  payloadType === protocol.IpcPayloadType.PING
    ||  payloadType === protocol.IpcPayloadType.PONG) {
      payload = utils.stringFromBuffer(payload);
    }

    else if (payloadType === protocol.IpcPayloadType.INT) {
      payload = utils.int32FromBufferLE(payload);
    }

    this.emit("notification", {
      payload,
      payloadType: protocol.payloadTypeToString(payloadType),
      address: utils.stringFromIPC(message.notification.address),
      msgId: utils.stringFromIPC(message.notification.msgid),
    });
  },
}, {
  type: protocol.IPCMessage.Type.RID,
  fn: function (message) {
    this.emit("rid", {
      address: utils.stringFromIPC(message.rid.response.address),
      rid: utils.stringFromIPC(message.rid.response.rid),
      secret: utils.stringFromIPC(message.rid.response.secret),
    });
  },
}, {
  type: protocol.IPCMessage.Type.APPLICATION_ID,
  fn: function (message) {
    const appId = utils.stringFromIPC(message.application_id.response.id);

    if (this._appIdRequest) {
      this._appIdRequest.resolve(appId);
      this._appIdRequest = null;
    }
  },
}, {
  type: protocol.IPCMessage.Type.GATEWAY_RID,
  fn: function (message) {
    this.emit("gateway-rid", {
      rid: utils.stringFromIPC(message.gateway_rid.rid),
    });
  },
}];

KSClient.constants = constants;
module.exports = KSClient;
