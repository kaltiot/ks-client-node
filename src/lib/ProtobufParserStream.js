// TODO: fix copypaste from ks-gateway

import _ from "lodash";
import stream from "readable-stream";
import { ByteBuffer } from "protobufjs";

export default class ProtobufParserStream extends stream.Transform {
  constructor(protocol) {
    super({ objectMode: true });
    this._protocol = protocol;
    this._clearBuffer();
  }

  _clearBuffer() {
    this.buffer = new Buffer(0);
    this.expectedLength = 0;
  }

  _transform(chunk, enc, callback) {
    // console.log("ProtobufParserStream: chunk of length", chunk.length, chunk);

    this.buffer = Buffer.concat([this.buffer, chunk]);
    this.parse();
    callback();
  }

  _flush(callback) {
    this.parse();
    callback();
  }

  parse() {
    while (this.buffer.length) {
      if (this.buffer.length && !this.expectedLength) {
        this._readVarInt();
      }

      if (this.buffer.length < this.expectedLength) {
        // console.log(
        //   "ProtobufParserStream:",
        //   `have ${this.buffer.length} bytes, expecting ${this.expectedLength}`
        // );
        break;
      }

      else if (this.buffer.length === this.expectedLength) {
        this._pushMessage();
        this._clearBuffer();
      }

      else if (this.buffer.length >= this.expectedLength) {
        this._pushMessage();
        this._storeRemainingBuffer();
      }
    }
  }

  _readVarInt() {
    try {
      const {
        length: varIntLength ,
        value: messageLength,
      } = ByteBuffer.wrap(this.buffer).readVarint32(0);

      this.expectedLength = varIntLength + messageLength;

      // console.log(
      //   "ProtobufParserStream: expected length is",
      //   this.expectedLength
      // );
    }

    catch (err) {
      // console.log("ProtobufParserStream: failed to read varint, " +
      //             "dropping one byte from buffer " +
      //             `(current length ${this.buffer.length})`,
      //             err);
      this.buffer = this.buffer.slice(1);
    }
  }

  _pushMessage() {
    const slice = this.buffer.slice(0, this.expectedLength);

    try {
      const message = this._protocol.decode(slice);
      this.push(message);
    }

    catch (err) {
      // console.log("ProtobufParserStream: decoding failed", err);
    }
  }

  _storeRemainingBuffer() {
    this.buffer = this.buffer.slice(this.expectedLength);
    this.expectedLength = 0;
  }
}
